var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    concat      = require('gulp-concat'),
    cleanCSS    = require('gulp-clean-css'),
    connect     = require('gulp-connect'),
    uglify      = require('gulp-uglify');



gulp.task('sass', function(){
    return gulp.src('css/*.sass')
    .pipe(sass())
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.stream());
});


gulp.task('styles', function() {
    return gulp.src(['css/bootstrap.css','css/jquery.fancybox.css','css/slick-theme.css','css/slick.css','css/plyr.css'])
        .pipe(concat('plugins_styles.min.css'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('./dist/'))
        .pipe(connect.reload());
});


gulp.task('scripts', function() {
    return gulp.src(['js/jquery.min.js','js/jquery.fancybox.min.js','js/slick.min.js','js/plyr.js'])
        .pipe(concat('plugins_scripts.min.js'))
        .pipe(uglify().on('error', function(e){
            console.log(e);
        }))
        .pipe(gulp.dest('./dist/'));
});


gulp.task('serve', gulp.series('sass', function() {
    browserSync.init({
        server: "."  
    });
    gulp.watch(['css/*.sass'], gulp.series('sass'));
    gulp.watch("*.html").on('change', browserSync.reload);
}));



gulp.task('default', gulp.parallel('sass', 'serve', 'styles', 'scripts'));




// gulp.task('browser-sync', function() {
//     browserSync.init({
//         server: {
//             baseDir: "app"
//         },
//         notify: false
//     });
// });




